const express = require('express');
const morgan = require('morgan');

const app = express();

const mongoose = require('mongoose');

mongoose.connect('mongodb+srv://NazariiKernytskyi:NazariiKernytskyi@cluster0.tkcjcia.mongodb.net/todoapp?retryWrites=true&w=majority');

const { notesRouter } = require('./notesRouter');
const { usersRouter } = require('./usersRouter');
const { authRouter } = require('./authRouter');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/notes', notesRouter);
app.use('/api/auth', authRouter);
app.use('/api/users', usersRouter);

const start = async () => {
  try {
    app.listen(8080);    
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

function errorHandler(err, req, res) {
  console.error(err);
  res.status(500).send({ message: 'Server error' });
}

app.use(errorHandler);
