const bcrypt = require('bcryptjs');
const { User } = require('./models/Users');

const getProfileInfo = (req, res) => {
  User.findById(req.user.userId).then((userInfo) => {
    res.status(200).json({ user: userInfo });
  });
};

const deleteUser = (req, res) => {
  User.findByIdAndDelete(req.user.userId).then(() => {
    res.status(200).json({ message: 'Success' });
  });
};

const changeProfilePassword = async (req, res) => {
  const { oldPassword, newPassword } = req.body;
  const user = await User.findOne({ username: req.user.username });

  if (await bcrypt.compare(String(oldPassword), String(user.password))) {
    return User.findOneAndUpdate(
      { username: req.user.username },
      { $set: { password: await bcrypt.hash(newPassword, 10) } },
    )
      .then(() => {
        res.status(200).json({ message: 'Success' });
      });
  }
  return res.json({ message: 'Wrong old password' });
};

module.exports = {
  getProfileInfo,
  deleteUser,
  changeProfilePassword,
};
