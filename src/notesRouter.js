const express = require('express');

const router = express.Router();
const {
  addUserNotes, getUserNotes, getUserNoteById, updateUserNoteById,
  toggleCompletedForUserNoteById, deleteUserNoteById,
} = require('./notesService');
const { authMiddleware } = require('./middleware/authMiddleware');

router.post('/', authMiddleware, addUserNotes);

router.get('/', authMiddleware, getUserNotes);

router.get('/:id', authMiddleware, getUserNoteById);

router.put('/:id', authMiddleware, updateUserNoteById);

router.patch('/:id', authMiddleware, toggleCompletedForUserNoteById);

router.delete('/:id', authMiddleware, deleteUserNoteById);

module.exports = {
  notesRouter: router,
};
