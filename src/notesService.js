const { Note } = require('./models/Note');

const addUserNotes = async (req, res) => {
  const { text } = req.body;
  const { userId } = req.user;
  const note = new Note({
    text,
    userId,
  });
  await note.save()
  .then(() => res.status(200).send({ message: 'Success' }),
  );
}

const getUserNotes = (req, res) => Note.find({ userId: req.user.userId }, '-__v')
.then((result) => {
  res.status(200).json({
    offset: 0,
    limit: 0,
    count: 0,
    notes: result,
  });
});

const getUserNoteById = (req, res) => Note.findById(req.params.id)
  .then((result) => {
    res.status(200).json({ note: result });
  });

const updateUserNoteById = (req, res) => {
  const { text } = req.body;
  return Note.findByIdAndUpdate({ _id: req.params.id, userId: req.user.userId }, { $set: { text } })
    .then(() => {
      res.status(200).json({ message: 'Success' });
    });
};

const toggleCompletedForUserNoteById = (req, res) => Note.findByIdAndUpdate(
  { _id: req.params.id, userId: req.user.userId },
  { $set: { completed: true } },
).then(() => {
  res.status(200).json({ message: 'Success' });
});

const deleteUserNoteById = (req, res) => Note.findByIdAndDelete(req.params.id)
  .then(() => {
    res.status(200).json({ message: 'Success' });
  });

module.exports = {
  addUserNotes,
  getUserNotes,
  getUserNoteById,
  updateUserNoteById,
  toggleCompletedForUserNoteById,
  deleteUserNoteById,
};
